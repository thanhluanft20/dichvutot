<form action="<?php echo osc_base_url(true); ?>" method="get"
      class="search nocsrf" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
    <input type="hidden" name="page" value="search"/>
    <div class="main-search">
        <div class="cell">
            <input type="text" name="sPattern" id="query" class="input-text" value=""
                   placeholder="<?php echo osc_esc_html(__(osc_get_preference('keyword_placeholder', 'bender_theme'),
                       'bender')); ?>"/>
        </div>
        <div class="cell selector tinh_thanh">
            <?php
            //Danh Sách Quốc Gia Sẽ Chạy
            $countrie_run = array("VN");

            //Lấy DS Quốc gia trong oc-class admin
            $countries = osc_get_countries();

            if (!empty($countries) && is_array($countries)):
                foreach ($countries as $key => $value) {
                    //Sử Lý Lựa Chọn Quốc Gia               !Close
                    $default_select_coutry = "VN";
                    $array_id_country = array();

                    //$value['pk_c_code'] = "VN";
                    if ((!empty($value['pk_c_code']) && in_array($value['pk_c_code'], $countrie_run)) &&
                        ($default_select_coutry == $value['pk_c_code'])
                    ) {
                        $regions = osc_get_regions($value['pk_c_code']); ?>
                        <select multiple="multiple" name="sregions" class="regions_<?php echo $value['pk_c_code']; ?>">
                            <option value="-1"><?php echo "Tất Cả"; ?></option>
                            <?php foreach ($regions as $keyr => $region) {
                                $array_id_country[] = $region['pk_i_id']; ?>
                                <option value="<?php echo $region['pk_i_id']; ?>"><?php echo $region['s_name']; ?></option>
                            <?php } ?>
                        </select>
                    <?php
                    }
                }
            else:
                die("Làm Ơn Thêm Quốc Gia Trong Admin");
            endif; ?>
        </div>
        <div class="cell selector quan_huyen">
            <div class="quan_huyen">
                <select name="scity[]" class="citys" multiple="multiple">
                    <option value="-1">Chọn Tỉnh Thành Trước</option>
                </select>
            </div>
        </div>
        <div class="cell reset-padding search">
            <button class="ui-button ui-button-big js-submit"><?php _e("Tìm Kiếm", 'bender'); ?></button>
        </div>
    </div>
    <div id="message-seach"></div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".citys").attr("disabled", true);
        //jquery select2
        $(".regions_VN").select2({
            placeholder: "Chọn Tỉnh Thành",
            maximumSelectionLength: 1
        });
        $(".citys").select2({
            placeholder: "Chọn Quận Huyện"
        });
        $(".regions_VN").change(function () {
            $select_regions = $(this).val();
            if ($select_regions != -1) {
                /*
                 load ajax
                 */
                var base_url = window.location;

                $.ajax({
                    "url": base_url + "index.php?page=ajax&action=cities&regionId=" + $select_regions,
                    "dataType": 'json',
                    success: function (json) {
                        //$(".citys").html("");
                        var html = '<option value="-1">Tất Cả</option>';
                        $.each(json, function (i, val) {
                            html += '<option value="' + val.pk_i_id + '">' + val.s_name + '</option>';
                        });
                        $(".citys").html(html);
                    }
                });

                $(".citys").attr("disabled", false);
                $(".citys").select2({
                    placeholder: "Chọn Quận Huyện"
                });
            } else {
                $(".citys").attr("disabled", true);
            }
        });
    });
</script>