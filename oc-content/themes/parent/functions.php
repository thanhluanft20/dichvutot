<?php


if(! class_exists('dvt_support')):
class dvt_support{
    function __construct(){
        osc_add_hook('init', array( $this,'dvt_load_scripts'));
    }

    function dvt_load_scripts(){

        osc_register_script('jquery-select2', osc_current_web_theme_url('select2/js/select2.min.js'), 'jquery');
        osc_enqueue_script('jquery-select2');
        osc_enqueue_style('css-select2', osc_current_web_theme_url('select2/css/select2.min.css'));
        osc_enqueue_style("main",  osc_current_web_theme_url("style.css"));
    }
}
endif;

new dvt_support;